package Zadania1;

import java.util.ArrayList;
import java.util.Scanner;

public class Zadania1Metody {
    // 1
    public static int CountNumber(int[] numbers, int checkNumber) {
        int counter = 0;
        for (int number : numbers) {
            if (checkNumber == number) {
                counter++;
            }
        }
        return counter;
    }

    //2
    public static int CountChar(String string,char check) {
        char[] charArray = string.toCharArray();
        int counter = 0;
        for (char ch: charArray) {
            if(ch == check) {
                counter++;
            }
        }
        return counter;
    }

    //3

    public static void CountMultiples() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a number which multiples you want to find");
        int a = scanner.nextInt();
        System.out.println("Enter a number (only multiples below this number will be displayed");
        int b = scanner.nextInt();
        System.out.println("All the multiples of " +a+ " below " +b+ ":");
        for(int i = 1; i <= b; i++) {
            if(a % i == 0) {
                System.out.format(" %d ", i );
            }
        }
    }
}
