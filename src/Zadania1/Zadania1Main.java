package Zadania1;
import static Zadania1.Zadania1Metody.*;

public class Zadania1Main {
    public static void main(String[] args) {
        // 1
        int[] array = {1,2,3,4,5,4};
        System.out.println("Number 4 appears in the array " + CountNumber(array, 4) + " times.");
        // 2
        System.out.println("Char 'b' appears in the string "+ CountChar("blebleble", 'b') + " times.");
        // 3
        CountMultiples();
    }
}
